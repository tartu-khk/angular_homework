var app = angular.module('exampleApp', ['ngRoute', 'ngAnimate']);
app.config(function($routeProvider) {
  $routeProvider
    .when('/example1', {
      templateUrl: "example.html",
      controller: "example1Ctrl"
    })
    .when('/example2', {
      templateUrl: "example.html",
      controller: "example2Ctrl"
    })
    .when('/example3', {
      templateUrl: "example.html",
      controller: "example3Ctrl"
    })
    .when('/example4', {
      templateUrl: "example.html",
      controller: "example4Ctrl"
    })
    .when('/example5', {
        templateUrl: "example.html",
        controller: "example5Ctrl"
    })
    .when('/example6', {
        templateUrl: "example.html",
        controller: "example6Ctrl"
    })
});

app.controller("example1Ctrl", function($scope, $route) {
  $scope.ex = {
    title: 'Building a table',
    code: `<html ng-app="countryApp">
    <head>
      <meta charset="utf-8">
      <title>Angular.js Example</title>
      <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.1/angular.min.js"></script>
      <script>
        var countryApp = angular.module('countryApp', []);
        countryApp.controller('CountryCtrl', function ($scope){
          $scope.countries = [
            {"name": "China", "population": 1359821000},
            {"name": "India", "population": 1205625000},
            {"name": "United States of America","population": 312247000}
          ];
        });
      </script>
    </head>
    <body ng-controller="CountryCtrl">
      <table>
        <tr>
          <th>Country</th>
          <th>Population</th>
        </tr>
        <tr ng-repeat="country in countries">
          <td>{{country.name}}</td>
          <td>{{country.population}}</td>
        </tr>
      </table>
    </body>
  </html>`,
    text: 'Lisab andmed tabelisse',
    imgurl: 'example1.png',
  }
});

app.controller("example2Ctrl", function($scope, $route) {
  $scope.ex = {
    title: 'Adding entries to a list using forms and ng-submit',
    code: `<html ng-app="nameApp">
    <head>
      <meta charset="utf-8">
      <title>Angular.js Example</title>
      <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.1/angular.min.js"></script>
      <script>
        var nameApp = angular.module('nameApp', []);
        nameApp.controller('NameCtrl', function ($scope){
          $scope.names = ['Larry', 'Curly', 'Moe'];
  
          $scope.addName = function() {
            $scope.names.push($scope.enteredName);
          };
        });
      </script>
    </head>
    <body ng-controller="NameCtrl">
      <ul>
        <li ng-repeat="name in names">{{name}}</li>
      </ul>
      <form ng-submit="addName()">
        <input type="text" ng-model="enteredName">
        <input type="submit" value="add">
      </form>
    </body>
  </html>`,
    text: 'Lisab listi kirjeid juurde',
    imgurl: 'example2.png',
  }
});

app.controller("example3Ctrl", function($scope, $route) {
  $scope.ex = {
    title: 'Adding capital data',
    code: `<html ng-app="countryApp">
    <head>
      <meta charset="utf-8">
      <title>Angular.js Example</title>
      <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.1/angular.min.js"></script>
      <script>
        var countryApp = angular.module('countryApp', []);
        countryApp.controller('CountryCtrl', function ($scope, $http){
          $http.get('countries.json').success(function(data) {
            $scope.countries = data;
          });
        });
      </script>
    </head>
    <body ng-controller="CountryCtrl">
      <table>
        <tr>
          <th>Country</th>
          <th>Population</th>
          <th>Flag</th>
          <th>Capital</th>
        </tr>
        <tr ng-repeat="country in countries">
          <td>{{country.name}}</td>
          <td>{{country.population}}</td>
          <td><img ng-src="{{country.flagURL}}" width="100"></td>
          <td>{{country.capital}}</td>
        </tr>
      </table>
    </body>
  </html>
  
  
  [
    {
      "name": "China",
      "population": 1359821000,
      "flagURL": "//upload.wikimedia.org/wikipedia/commons/f/fa/Flag_of_the_People%27s_Republic_of_China.svg",
      "capital": "Beijing"
    },
    {
      "name": "India",
      "population": 1205625000,
      "flagURL": "//upload.wikimedia.org/wikipedia/en/4/41/Flag_of_India.svg",
      "capital": "New Delhi"
    },
    {
      "name": "United States of America",
      "population": 312247000,
      "flagURL": "//upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg",
      "capital": "Washington, D.C."
    }
  ]`,
    text: 'Võtab JSON failist andmed ja kuvab need tabelis',
    imgurl: 'example3.png',
  }
});

app.controller("example4Ctrl", function($scope, $route) {
  $scope.ex = {
    title: 'Caching JSON data in a service',
    code: `<html ng-app="countryApp">
    <head>
      <meta charset="utf-8">
      <title>Angular.js Example</title>
      <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.10/angular.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.10/angular-route.min.js"></script>
      <script>
        var countryApp = angular.module('countryApp', ['ngRoute']);
  
        countryApp.config(function($routeProvider) {
          $routeProvider.
            when('/', {
              templateUrl: 'country-list.html',
              controller: 'CountryListCtrl'
            }).
            when('/:countryName', {
              templateUrl: 'country-detail.html',
              controller: 'CountryDetailCtrl'
            }).
            otherwise({
              redirectTo: '/'
            });
        });
  
        countryApp.factory('countries', function($http){
  
          var cachedData;
  
          function getData(callback){
            if(cachedData) {
              callback(cachedData);
            } else {
              $http.get('countries.json').success(function(data){
                cachedData = data;
                callback(data);
              });
            }
          }
  
          return {
            list: getData,
            find: function(name, callback){
              getData(function(data) {
                var country = data.filter(function(entry){
                  return entry.name === name;
                })[0];
                callback(country);
              });
            }
          };
        });
  
        countryApp.controller('CountryListCtrl', function ($scope, countries){
          countries.list(function(countries) {
            $scope.countries = countries;
          });
        });
  
        countryApp.controller('CountryDetailCtrl', function ($scope, $routeParams, countries){
          countries.find($routeParams.countryName, function(country) {
            $scope.country = country;
          });
        });
      </script>
    </head>
    <body>
      <div ng-view></div>
    </body>
  </html>
  
  
  <h1>{{country.name}}</h1>
<ul>
  <li>Flag: <img ng-src="{{country.flagURL}}" width="100"></li>
  <li>Population: {{country.population | number }}</li>
  <li>Capital: {{country.capital}}</li>
  <li>GDP: {{country.gdp | currency }}</li>
</ul>



<ul>
  <li ng-repeat="country in countries">
    <a href="#/{{country.name}}">{{country.name}}</a>
  </li>
</ul>



[
  {
    "name": "China",
    "population": 1359821000,
    "flagURL": "//upload.wikimedia.org/wikipedia/commons/f/fa/Flag_of_the_People%27s_Republic_of_China.svg",
    "capital": "Beijing",
    "gdp": 12261
  },
  {
    "name": "India",
    "population": 1205625000,
    "flagURL": "//upload.wikimedia.org/wikipedia/en/4/41/Flag_of_India.svg",
    "capital": "New Delhi",
    "gdp": 4716
  },
  {
    "name": "United States of America",
    "population": 312247000,
    "flagURL": "//upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg",
    "capital": "Washington, D.C.",
    "gdp": 16244
  }
]`,
    text: 'Cacheb JSON andmeid',
    imgurl: 'example4.png',
  }
});

app.controller("example5Ctrl", function($scope, $route) {
    $scope.ex = {
     title: 'Sorting table columns interactively',
     code: `<html ng-app="countryApp">
     <head>
       <meta charset="utf-8">
       <title>Angular.js Example</title>
       <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.1/angular.min.js"></script>
       <script>
         var countryApp = angular.module('countryApp', []);
         countryApp.controller('CountryCtrl', function ($scope, $http){
           $http.get('countries.json').success(function(data) {
             $scope.countries = data;
           });
   
           $scope.sortField = 'population';
         });
       </script>
     </head>
     <body ng-controller="CountryCtrl">
       Search:<input ng-model="query" type="text"/>
       <table>
         <tr>
           <th><a href="" ng-click="sortField = 'name'">Country</a></th>
           <th><a href="" ng-click="sortField = 'population'">Population</a></th>
         </tr>
         <tr ng-repeat="country in countries | filter:query | orderBy:sortField">
           <td>{{country.name}}</td>
           <td>{{country.population}}</td>
         </tr>
       </table>
     </body>
   </html>`,
     text: '`Sorteerib andmeid JSON failist`',
     imgurl: 'example5.png',
    }
});

app.controller("example6Ctrl", function($scope, $route) {
    $scope.ex = {
     title: 'Looping over lists in templates using ng-repeat',
     code: `<html ng-app="nameApp">
     <head>
       <meta charset="utf-8">
       <title>Angular.js Example</title>
       <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.1/angular.min.js"></script>
       <script>
         var nameApp = angular.module('nameApp', []);
         nameApp.controller('NameCtrl', function ($scope){
           $scope.names = ['Larry', 'Curly', 'Moe'];
         });
       </script>
     </head>
     <body ng-controller="NameCtrl">
       <ul>
         <li ng-repeat="name in names">{{name}}</li>
       </ul>
     </body>
   </html>`,
     text: 'Väljastab andmed arrayst',
     imgurl: 'example6.png',
    }
});