<?php require 'header.php'; ?>

<section class="content-block content-1-2">
    <div class="container" ng-app="exampleApp">

        <!-- Example template -->
        <script type="text/ng-template" id="example.html">
            <div class="row">
                <div class="col-sm-6">
                    <p class="lead">{{ex.title}}</p>
                    <pre>{{ex.code}}</pre>
                </div>
                <div class="col-sm-6 col-md-6">
                    <img class="img-rounded img-responsive" src="img\{{ex.imgurl}}">
                </div>
            </div>
            <!-- Start Row -->
            <div class="row">
                <div class="col-sm-6">
                    <p>{{ex.text}}</p>
                </div>
                <div class="col-sm-6"></div>
            </div>
        </script>

        <!-- Start Row -->
        <div class="row">
            <div class="col-sm-auto col-md-auto col-lg-auto">
                <h2>Angular.js näited</h2> 
            </div>
        </div>
        <!-- Start Row -->
        <div class="row">
            <div class="col-xs-12 col-sm-2 col-6">
                <a href="#!/example1" class="btn btn-warning btn-block">Näide 1<span class="fa fa-check"></span></a>
            </div>
            <div class="col-xs-12 col-sm-2 col-6">
                <a href="#!/example2" class="btn btn-warning btn-block">Näide 2<span class="fa fa-check"></span></a>
            </div>
            <div class="col-xs-12 col-sm-2 col-6">
                <a href="#!/example3" class="btn btn-warning btn-block">Näide 3<span class="fa fa-check"></span></a>
            </div>
            <div class="col-xs-12 col-sm-2 col-6">
                <a href="#!/example4" class="btn btn-warning btn-block">Näide 4<span class="fa fa-check"></span></a>
            </div>
            <div class="col-xs-12 col-sm-2 col-6">
                <a href="#!/example5" class="btn btn-warning btn-block">Näide 5<span class="fa fa-check"></span></a>
            </div>
            <div class="col-xs-12 col-sm-2 col-6">
                <a href="#!/example6" class="btn btn-warning btn-block">Näide 6<span class="fa fa-check"></span></a>
            </div>
        </div>
        <!-- Start Row -->
        <div class="exampleView" ng-view></div>
        
    </div>
</section>

<?php require 'footer.php'; ?>
