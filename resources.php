<?php require 'header.php'; ?>

<section id="content-1-2" class="content-block content-1-2">
    <div class="container">
        <!-- Start Row -->
        <div class="row">
            <div class="col-sm-auto col-md-auto col-lg-auto">
                <h2>Angular.js materjalid</h2> 
            </div>
        </div>
        <!-- Start Row -->
        <div class="row">
            <div class="col-md-12">
                <table class="table"> 
                    <thead> 
                        <tr> 
                            <th>#</th> 
                            <th>Materjal</th> 
                            <th>Tutvustus</th> 
                            <th>Link</th> 
                        </tr>                                 
                    </thead>                             
                    <tbody> 
                         <tr> 
                            <th scope="row">1</th> 
                            <td>Angular 6 (formerly Angular 2) - The Complete Guide </td> 
                            <td>Master Angular (Angular 2+, incl. Angular 6) and build awesome, reactive web apps with the successor of Angular.js </td> 
                            <td><a href="https://www.udemy.com/the-complete-guide-to-angular-2/?siteID=JVFxdTr9V80-Sxo5SUdkyx8moobgV_VvYQ&LSNPUBID=JVFxdTr9V80">Udemy course</a></td> 
                        </tr>                                 
                        <tr> 
                            <th scope="row">2</th> 
                            <td>Learn and Understand AngularJS </td> 
                            <td>Master AngularJS and the Javascript concepts behind it, design custom directives, and build a single page application. </td> 
                            <td><a href="https://www.udemy.com/learn-angularjs/?siteID=JVFxdTr9V80-4SmFC9K0lBv9MztzCLldoQ&LSNPUBID=JVFxdTr9V80">Udemy course</a></td> 
                        </tr>                                 
                        <tr> 
                            <th scope="row">3</th> 
                            <td>Tutorial: Tour of Heroes</td> 
                            <td>The Tour of Heroes tutorial covers the fundamentals of Angular. In this tutorial you will build an app that helps a staffing agency manage its stable of heroes.</td> 
                            <td><a href="https://angular.io/tutorial">Official tutorial</a></td> 
                        </tr>
                        <tr> 
                            <th scope="row">4</th> 
                            <td>Getting Started with Angular 2+ </td> 
                            <td>Learn how to build your first Angular 2+ app! </td> 
                            <td><a href="https://www.udemy.com/getting-started-with-angular-2/?siteID=JVFxdTr9V80-JCf1rR4iO98Rt63oRY7DXg&LSNPUBID=JVFxdTr9V80">Udemy course</a></td> 
                        </tr>
                        <tr> 
                            <th scope="row">5</th> 
                            <td>Angular: Getting Started</td> 
                            <td> Angular is one of the fastest, most popular open source web app frameworks today, and knowing how to use it is essential for developers. You'll learn how to create components and user interfaces, data-binding, retrieving data using HTTP, and more.</td> 
                            <td><a href="https://www.pluralsight.com/courses/angular-2-getting-started-update?clickid=QwM0a2XQ6VGZQSo1vp1DbwTIUkgzLn3Cx2sVWQ0&irgwc=1&mpid=1193463&utm_source=impactradius&utm_medium=digital_affiliate&utm_campaign=1193463&aid=7010a000001xAKZAA2">Pluralsight</a></td> 
                        </tr>                                 
                    </tbody>                             
                </table>
            </div>
        </div>
    </div>
</section>

<?php require 'footer.php'; ?>