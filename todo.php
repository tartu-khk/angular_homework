<?php require 'header.php'; ?>

<section class="content-block content-1-2">
    <div class="container" ng-app="todoApp">
        <!-- Start Row -->
        <div class="row">
            <div class="col-sm-auto col-md-auto col-lg-auto">
                <h2>Angular.js todo</h2> 
            </div>
        </div>
        <!-- Start Row -->
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div ng-controller="TodoController">
                    <span>Tegemata: {{remaining()}}</span><br>
                    <span>Kokku: {{todos.length}}</span><br>
                    [ <a href="" ng-click="archive()">arhiveeri</a> ]
                    <ul class="unstyled">
                        <li ng-repeat="todo in todos">
                            <input type="checkbox" ng-model="todo.done">
                            <span class="done-{{todo.done}}" ng-click="halo()">{{todo.text}}</span>
                        </li>
                    </ul>
                    <form ng-submit="addTodo()">
                        <!-- Start Row -->
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <input type="text" ng-model="todoText" size="30" placeholder="lisa uus todo" class="form-control">
                            </div>
                        </div>
                        <!-- Start Row -->
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <input class="btn btn-block btn-primary col-md-3" type="submit" value="lisa">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
        
<?php require 'footer.php'; ?>