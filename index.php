<?php require 'header.php'; ?>

<section class="content-block content-1-2">
    <div class="container">
        <!-- Start Row -->
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h2>Angular.js õppekeskkond</h2>
            </div>
            <div class="col-xs-12 col-sm-3">
                <a href="https://angular.io/docs" class="btn btn-block btn-primary col-md-3">Angular Docs</a>
            </div>
                <div class="col-xs-12 col-sm-3">
            <a href="https://github.com/andressoop/angular_homework" class="btn btn-block btn-warning">Github link</a>
            </div>
        </div>
        <!-- Start Row -->
        <div class="row">
            <div class="col-sm-6">
                <p class="lead">Mis on Angular.JS?</p>
                <p><i>AngularJS (<a href="https://angularjs.org/">angularjs.org</a>)
                on 2009 aastal loodud Javascriptil baseeruv veebirakenduste
                raamistik. Selle abil saab laiendada HTML süntaksit, muutes selle
                kasutaja jaoks reaalajas reageerivaks. N:  kui klient vajutab veebilehel
                linki, siis ei laeta alla kogu lehte vaid mingi osa (Single Page
                Application).</i></p>
            </div>
            <div class="col-sm-6 col-md-6">
                <img class="img-rounded img-responsive" src="img/angular-logo.png">
            </div>
        </div>
        <!-- Start Row -->
        <div class="row">
            <div class="col-sm-6">
                <p class="lead">Mis on<i> </i>MVC?</p>
                    <p><p><i>MVC (Model-View-Controller) arhitektuur jagab objektid kolmeks (mudel, vaade, kontroller) ja määrab ära, kuidas need omavahel suhtlevad. </i><br></p>
                        <ul id="yui_3_17_2_1_1538490426830_55">
                            <li>
                                <i><strong>mudel</strong> <em>(model)</em> – hoiab endas andmeid ja määrab ära kuidas andmeid töödeldakse.</i>
                            </li>
                            <li>
                                <i><strong>vaade</strong> <em>(view)</em> – kõik mida kasutaja ekraanil näeb. Mida kuvada saadakse teada kontrollerilt.</i>
                            </li>
                            <li id="yui_3_17_2_1_1538490426830_54">
                                <i><strong>kontroller</strong> <em>(controller) </em>–
                                    organiseerib mudel ja vaade omavahelist tööd. Kui andmed muutuvad, siis
                                    kontroller teavitab sellest vaadet. Kontroller tegeleb ka kasutaja
                                    tegevuse tõlkimisega.</i>
                            </li>
                        </ul>
                    </p>
            </div>
            <div class="col-sm-6 col-md-6">
                <img class="img-rounded img-responsive" src="img/MVC.JPG">
            </div>
        </div>
    </div>
</section>

<?php require 'footer.php'; ?>
