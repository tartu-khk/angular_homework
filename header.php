<!DOCTYPE html>
<html lang="et">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/x-icon" href="https://angular.io/favicon.ico">
        <title>Angular.js - Andres Soop VSo17</title>

        <!-- Angular -->
        <script src="assets/angular.js"></script>
        <script src="assets/angular-route.js"></script>
        <script src="assets/angular-animate.js"></script>
        <script src="assets/todo.js"></script>
        <script src="assets/example.js"></script>
        <script src="assets/game.js"></script>
        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="justified-nav.css" rel="stylesheet">
        <link href="assets/style.css" rel="stylesheet">
        <link rel="stylesheet" href="components/pg.blocks/css/blocks.css">
        <link rel="stylesheet" href="components/pg.blocks/css/plugins.css">
        <link rel="stylesheet" href="components/pg.blocks/css/style-library-1.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <div class="masthead">
                <h3 class="text-muted">Angular.js</h3>
                <nav>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarCollapse">
                        <ul class="nav nav-pills nav-justified w-100">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php">Avaleht</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="todo.php">Todo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="example.php">Näited</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="resources.php">Materjalid</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="game.php">Mäng</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="feedback.php">Tagasiside</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>         