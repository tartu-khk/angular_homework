<?php require 'header.php'; ?>

<section id="content-1-2" class="content-block content-1-2">
    <div class="container">
        <!-- Start Row -->
        <div class="row">
            <div class="col-sm-auto col-md-auto col-lg-auto">
                <h2>Angular.js kontakt</h2>
            </div>
        </div>
        <!-- Start Row -->
        <div class="row">
            <div class="col-md-12">
                <div id="contact" class="form-container text-center">
                    <div id="message"></div>
                    <form method="post" action="email.php" name="contactform" class="contactform">
                        <!-- Start Row -->
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input name="name" id="name" type="text" value="" placeholder="Nimi" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input name="email" id="email" type="email" value="" placeholder="E-Post" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input name="phone" id="phone" type="tel" value="" class="form-control" placeholder="Telefon"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="message" class="form-control" rows="3" placeholder="Sõnum" id="textArea" required></textarea>
                        </div>
                        <div class="form-group">
                            <input class="btn btn-primary" type="submit" id="cf-submit" name="submit" value="Saada">
                        </div>
                        <div class="alert alert-success contactmsg" style="display: none" role="alert">
                            Kiri edukalt saadetud.
                        </div>
                    </form>
                </div>                         
            </div>
        </div>
    </div>
</section>

<?php require 'footer.php'; ?>
